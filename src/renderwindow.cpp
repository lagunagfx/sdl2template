#include <iostream>
#include <SDL.h>
#include "renderwindow.h"

renderWindow::renderWindow(const char* p_title, int p_width, int p_height )
	:window(NULL), renderer(NULL)
{
	window = SDL_CreateWindow( p_title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, p_width, p_height, SDL_WINDOW_SHOWN );

	if ( window == NULL )
		std::cout << "ERROR: Window failed to init. SDL_ERROR " << SDL_GetError() << std::endl;
	
	renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
}

void renderWindow::cleanup() {
	SDL_DestroyWindow(window);
}

void renderWindow::clear() {
	SDL_RenderClear(renderer);
}

void renderWindow::display() {
	SDL_RenderPresent(renderer);
}

void renderWindow::background( int p_r, int p_g, int p_b, int p_transparency ) {
	SDL_SetRenderDrawColor( renderer, p_r, p_g, p_b, p_transparency ); 
}
