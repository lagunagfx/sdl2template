#pragma once
#include <SDL.h>

class renderWindow {
	public:
		renderWindow( const char* p_title, int p_width , int p_height );
		void cleanup();
		void clear();
		void display();
		void background( int p_r, int p_g, int p_b, int transparency );
	private:
		SDL_Window* window;
		SDL_Renderer* renderer;
};
