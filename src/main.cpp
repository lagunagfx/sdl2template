/*
	My first C++ program using SDL
*/

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
 
#include "renderwindow.h"
#include "entity.h"

int main( int argc, char* args[] ) {
	if ( SDL_Init(SDL_INIT_VIDEO) > 0)
		std::cout << "ERROR: SDL_Init() HAS FAILED with SDL_ERROR " << SDL_GetError() << std::endl;

	renderWindow window("My Game", 640, 480);

	bool programRunning = true;
	SDL_Event event;

	while( programRunning ) {

		while( SDL_PollEvent(&event) ) {
			if (event.type == SDL_QUIT)
				programRunning = false;
		}				

		window.clear();

		for ( int x = 0; x<256; x++ ) { 
			window.background(x,255-x,0,255);
			if ( x > 255 )
				x = 0;
		}

		window.display();
	}

	window.cleanup();
	SDL_Quit();
	return 0;	
}
