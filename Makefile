# -*- MakeFile -*-

TARGET = output
SRC = main.cpp renderwindow.cpp entity.cpp
OBJ = main.o renderwindow.o entity.cpp
DEPS = renderwindow.h entity.h

CXX = clang++
CFLAGS= -Wall -g -O3 $(shell sdl2-config --cflags)
LDFLAGS = $(shell sdl2-config --libs)

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CXX) $(CFLAGS) $(LDFLAGS) $(OBJ) -o $(TARGET)

%.o: %.cpp $(DEPS)
	$(CXX) $(CFLAGS) -c $< -o $@

.PHONY: install clean

clean:
	rm -v $(OBJ) $(TARGET)
